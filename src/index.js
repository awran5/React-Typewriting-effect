import React from 'react'
import ReactDOM from 'react-dom'
import TypeWriter from './TypeWriter'

import './styles.css'

function App() {
	return (
		<div className="App" style={{ marginTop: '5rem' }}>
			<h1>
				I Love{' '}
				<span style={{ color: 'red' }}>
					<TypeWriter
						loop={true}
						speed={100} // Speed in ms
						delay={1500} // Delay after deleting effect
						words={[ 'Node.js', 'React.js', 'Gatsby.', 'JavaSctipt!' ]}
					/>
				</span>
			</h1>
		</div>
	)
}

const rootElement = document.getElementById('root')
ReactDOM.render(<App />, rootElement)
