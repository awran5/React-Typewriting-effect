import React, { useState, useCallback, useEffect } from 'react'
import propTypes from 'prop-types'

const TypeWriter = ({ loop, speed, delay, words, style }) => {
	const [ counter, setCounter ] = useState(0)
	const [ text, setText ] = useState('')
	const [ isDeleting, setIsDeleting ] = useState(false)
	const [ typingSpeed, setTypingSpeed ] = useState(100)

	const handleTyping = useCallback(
		() => {
			const index = loop ? counter % words.length : counter
			const fullWord = words[index]
			setTypingSpeed(speed)

			if (isDeleting) {
				setTypingSpeed(speed / 2)
				setText(fullWord.substring(0, text.length - 1))
			} else {
				setText(fullWord.substring(0, text.length + 1))
			}

			if (!isDeleting && text === fullWord) {
				if (!loop && counter >= words.length - 1) return
				setIsDeleting(true)
				setTypingSpeed(delay)
			} else if (isDeleting && text === '') {
				setIsDeleting(false)
				setCounter(counter + 1)
			}
		},
		[ counter, delay, isDeleting, loop, speed, text, words ]
	)

	useEffect(
		() => {
			const timer = setTimeout(() => handleTyping(), typingSpeed)
			return () => clearTimeout(timer)
		},
		[ handleTyping, typingSpeed ]
	)

	return (
		<span loop={loop} speed={speed} delay={delay} words={words} style={style}>
			<span>{text}</span>
			<span className="blinking-cursor">|</span>
		</span>
	)
}
TypeWriter.propTypes = {
	loop: propTypes.bool,
	speed: propTypes.number,
	delay: propTypes.number,
	words: propTypes.array,
	style: propTypes.object
}
export default TypeWriter
