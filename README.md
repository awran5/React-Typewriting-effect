# React Typewriter Effect
Simple React component to add typewriting effect to your site.
## Usage: 

```jsx
import TypeWriter from "./TypeWriter";
```
```jsx
<span style={{ color: "red" }}>
  <TypeWriter
    loop={true}
    speed={100}
    delay={1500}
    words={["word 1", "word 2.", "word 3", "etc.."]}
  />
</span>
```

## Props: 

**loop:** [`true, false`] (default: `true`)

**speed:** Type: [`Number`] Writing Speed in MS i.e. `100` (default: `100`)

**delay:** Type: [`Number`] Delay before deleting text in MS i.e. `1000` (default: `1500`)

**words:** Type: [`Array`] Array of your words.

<hr />


### [codeSandBox](https://codesandbox.io/s/react-typewriting-effect-8ulgs)
### [Demo] (https://8ulgs.csb.app/)
### [Live example](https://gkstyle.net/)
